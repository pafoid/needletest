package com.nemator.needle.home.task.createHaystack;

import com.nemator.needle.models.Haystack;

public class CreateHaystackResult {
    public int successCode;
    public String message;
    public Haystack haystack;
}
